apt-get -y install zsh
apt-get -y install vim
apt-get -y install curl
apt-get -y install net-tools
rm -rf /root/.zcompdump*
chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
echo "syntax on\nset mouse=a" >> .vimrc
cat .zshrc | sed "s/ZSH_THEME=\"robbyrussell\"/ZSH_THEME=\"af-magic\"/g" > .zshrc
