#!/bin/bash

CRON_1=/etc/crontab
FILE=/root/.cron_tmp

if [ ! -f $FILE ]
then
	cp $CRON_1 $FILE
fi

DIFF=`diff $CRON_1 $FILE | wc -l`

if [ $DIFF -ge "1" ]
then
	DATE=`date`
	CAT=`cat $FILE`
	echo "[ $DATE ]\nthe crontab file $CRON_1 as changed\n\n$FILE" > mail.txt
	mail -s "[debian] cron as changed!" root@debian < mail.txt
	cp $CORN_1 $FILE
	rm -rf mail.txt
fi
