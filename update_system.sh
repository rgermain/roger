#!/bin/bash

FILE="/var/log/update_script.log"
DATE=`date`

if [ -f $FILE ]
then
	printf "\n"
fi

printf "[ $DATE ]\n" >> $FILE
apt-get -y update && apt-get -y upgrade >> $FILE
