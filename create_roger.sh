#!/bin/bash

echo "[ create all you need in roger ]\n"
apt-get update -y && apt-get upgrade -y
apt-get install -y fail2ban
apt-get install -y iptables-persistent

###             cron tab    ####
echo "	[ create crontab ]"
CRON="/etc/crontab"
echo "@reboot		root	/root/roger/update_system.sh" >> $CRON
echo "0 4 	* * 1	root	/root/roger/update_system.sh" >> $CRON
echo "0 0	* * *  	root	/root/roger/cron_checker.sh" >> $CRON
###            end cron tab    ####


###            ssh    ####
echo "	[ create ssh ]"

SSH="/etc/ssh/sshd_config"
echo "port 2222" >> $SSH
echo "PermitRootLogin no" >> $SSH
echo "PasswordAuthentication yes" >> $SSH
echo "PubkeyAuthentication yes" >> $SSH
###            end ssh    ####


###           interfaces    ####
echo "	[ create network bridges ]"

NET="/etc/network/interfaces"
echo "###            end tab    ####"
echo "# This file describes the network interfaces available on your system" > $NET
echo "# and how to activate them. For more information, see interfaces(5)." >> $NET
echo "source /etc/network/interfaces.d/*" >> $NET
echo "# The loopback network interface" >> $NET
echo "auto lo" >> $NET
echo "iface lo inet loopback" >> $NET
echo "# The primary network interface" >> $NET
echo "auto enp0s3" >> $NET
echo "iface enp0s3 inet static" >> $NET
echo "address 10.0.2.15" >> $NET
echo "netmask 255.255.255.252" >> $NET
echo "gateway 10.0.2.3" >> $NET
###      end     interfaces    ####


###            firewall    ####
echo "	[ create firewall rules ]"

IP_TABLE="/etc/network/if-pre-up.d/iptables"
echo "iptables -P INPUT DROP\niptables -A INPUT -i lo -j ACCEPT\niptables -A INPUT -p tcp -i enp0s3 --dport 2222 -j ACCEPT\niptables -A INPUT -p tcp -i enp0s3 --dport 80 -j ACCEPT\niptables -A INPUT -p tcp -i enp0s3 --dport 443 -j ACCEPT" >> $IP_TABLE
###            end firewall    ####

###            scan    ####
echo "	[ create protection scan fail2ban ]"

FAIL="/etc/fail2ban/jail.d/defaults-debian.conf"
echo "[sshd]\nenabled = true\nport = 2222\n\n[sshd-ddos]\nport = 2222\nenabled = true\n" >> $FAIL
###            end protection scan    ####

