#!/bin/bash
chsh -s $(which bash)
bash
apt-get -y remove --purge zsh
apt-get -y remove --purge vim
apt-get -y remove --purge curl
apt-get -y remove --purge net-tools
rm -rf /root/.zcompdump*
rm -rf /root/.zshrc /root/.oh-my-zsh
