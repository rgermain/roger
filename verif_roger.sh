#!/bin/bash

NB="0"
SPATH="/root/roger"

	getch()
	{
		green
		printf "\n\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-"
		reset
		printf "\n\npress  [ENTER]  to continue deleting line\n"
		read continue
	}

	red()
	{
		printf "\033[31m"
	}

	blue()
	{
		printf "\033[34m"
	}
	
	grey()
	{
		printf "\033[37m"
	}
	
	green()
	{
		printf "\033[38;5;226m"
	}
	
	yellow()
	{
		printf "\033[32m"
	}

	black()
	{
		printf "\033[31m"
	}

	reset()
	{
		printf "\033[0m"
	}

	title()
	{
		grey
		printf "\n\t\t[  $1 ]\n\n"
		blue
	}
	
	title2()
	{
		grey
		printf "\n\t[  $1 ]\n\n"
		blue
	}


	com()
	{
		reset
		printf "commande [$NB] |  \""
		red
		printf " $1 "
		reset
		printf "\"\n"
		NB=$((NB+1))
		blue
	}

green
printf "\n\n\t\t[   verif scirpt for roger skyline ]\n\n"
reset
getch

title "check partition  4.2gb "
com "df -H /dev/sda1"

df -H /dev/sda1

getch
title "check Crontab"
cat /etc/crontab

getch
title "view script"
title2 "script update system"
cat $SPATH/update_system.sh
title2 "script check crontab "
cat $SPATH/cron_checker.sh

getch
title "check sshd"
com " cat /etc/ssh/sshd_config | grep -i -e \"Port \" -e \"PermitRoot\" -e \"PasswordAu\" -e \"PubkeyA\" | grep -v \"#\""
cat /etc/ssh/sshd_config | grep -i -e "Port " -e "PermitRoot" -e "PasswordAu" -e "PubkeyA" | grep -v "#"

getch
title "check interface"
cat /etc/network/interfaces

getch
title " firewall && fail2ban"
title2 "open port"
com "netstat -paunt"
netstat -paunt
title2 "fail2ban status"
com "fail2ban-client status"
fail2ban-client status
title2 "iptables"
com "iptables -L"
iptables -L
title2 "ssh"
com "cat /etc/fail2ban/jail.d/defaults-debian.conf"
cat /etc/fail2ban/jail.d/defaults-debian.conf

